exports.Index = function(request, response){
    response.pageInfo.title = 'Webstorage';
    response.render('webstorage/Index', response.pageInfo);
};

exports.LocalStorage = function(request, response){
    response.pageInfo.title = 'LocalStorage';
    response.render('webstorage/LocalStorage', response.pageInfo);
};

exports.SessionStorage = function(request, response){
    response.pageInfo.title = 'SessionStorage';
    response.render('webstorage/SessionStorage', response.pageInfo);
};
