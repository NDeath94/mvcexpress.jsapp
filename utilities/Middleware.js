exports.AppendNotifications = function(request, response, next) {

    response.pageInfo = {};
    response.pageInfo.notifications = {};
    response.pageInfo.notifications.message = '';

    if(request.query.message) {
        response.pageInfo.notifications.message = request.query.message;
    }

    if(request.query.success) {
        response.pageInfo.notifications.success = "Success!";
    }
    else if (request.query.error){
        response.pageInfo.notifications.error = "Error!";
    }

    next();

};
