var mongoose = require('mongoose');

var Book = new mongoose.Schema({
    title: String,
    author: { type: mongoose.Schema.Types.ObjectId, ref: 'Author' },
    isPublished: Boolean
});

var Author = new mongoose.Schema({
    name: String
});

var User = new mongoose.Schema({
    name: String,
    password: String,
    email: String
});

var BookModel = mongoose.model('Book', Book);
var AuthorModel = mongoose.model('Author', Author);
var UserModel = mongoose.model('User', User);


module.exports = {
    BookModel: BookModel,
    AuthorModel: AuthorModel,
    UserModel: UserModel
};
