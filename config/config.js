var config = {};

config.development = {

    database: {
        name: 'defaultdb',
        host: '@ds137530.mlab.com',
        port: '37530',
        credentials: 'admin:admin'
    },
    application: {
        port: 3000
    }

};

config.production = {

    database: {
        name: 'defaultdb',
        host: '@ds137530.mlab.com',
        port: '37530',
        credentials: 'admin:admin'
    },
    application: {
        port: 80
    }

};

config.environment = 'development';

module.exports = config;
