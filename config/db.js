var config = require('./config');
var mongoose = require('mongoose');
var uri = 'mongodb://' + config[config.environment].database.credentials +config[config.environment].database.host + ':' + config[config.environment].database.port + '/' + config[config.environment].database.name;
var db = mongoose.connection;
db.on('error', function(){
    console.log('There was an error connecting to the database');
});

db.once('open', function() {
    console.log('Successfully connected to database');
});

const options = {
    useMongoClient: true,
    autoIndex: false, // Don't build indexes
    reconnectTries: Number.MAX_VALUE, // Never stop trying to reconnect
    reconnectInterval: 500, // Reconnect every 500ms
    poolSize: 10, // Maintain up to 10 socket connections
    // If not connected, return errors immediately rather than waiting for reconnect
    bufferMaxEntries: 0
};

mongoose.connect(uri, options);