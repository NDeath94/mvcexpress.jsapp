//LocalStorage functions
function saveLocalStorage () {
    var input = document.getElementById("input");
    localStorage.setItem("localStorage", input.value);
    loadLocalStorage()
}

function loadLocalStorage () {
    if (localStorage.getItem("localStorage")) {
        document.getElementById("output").innerHTML = localStorage.getItem("localStorage");
    }
}

//SessionStorage functions
function saveSessionStorage () {
    var input = document.getElementById("input");
    sessionStorage.setItem("sessionStorage", input.value);
    loadSessionStorage()
}

function loadSessionStorage () {
    if (sessionStorage.getItem("sessionStorage")) {
        document.getElementById("output").innerHTML = sessionStorage.getItem("sessionStorage");
    } 
}