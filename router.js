var HomeController = require('./controllers/HomeController');
var BookController = require('./controllers/BookController');
var AuthorController = require('./controllers/AuthorController');
var WebstorageController = require('./controllers/WebstorageController');
// Routes
module.exports = function(app){

    // Main Routes
    app.get('/', HomeController.Index);
    app.get('/other', HomeController.Other);

    // Book Routes
    app.get('/books', BookController.Index);
    app.get('/books/add', BookController.BookAdd);
    app.post('/books/add', BookController.BookCreate);
    app.get('/books/edit/:id', BookController.BookEdit);
    app.post('/books/edit', BookController.BookUpdate);
    app.get('/books/delete/:id', BookController.BookDelete);
    
    // Author Routes
    app.get('/authors', AuthorController.Index);
    app.get('/authors/add', AuthorController.AuthorAdd);
    app.post('/authors/add', AuthorController.AuthorCreate);
    app.get('/authors/edit/:id', AuthorController.AuthorEdit);
    app.post('/authors/edit', AuthorController.AuthorUpdate);
    app.get('/authors/delete/:id', AuthorController.AuthorDelete);

    // Webstorage Routes
    app.get('/webstorage', WebstorageController.Index);
    app.get('/webstorage/LocalStorage', WebstorageController.LocalStorage);
    app.get('/webstorage/SessionStorage', WebstorageController.SessionStorage);
};
