// app.js
// modules =================================================
var express = require('express'); //Loads up the express module.
var http = require('http');
var path = require('path');
var app = express();

//middleware & utilities ===================================
var logger = require('morgan');
var errorHandler = require('errorhandler');
var bodyParser = require('body-parser')
var Middleware = require('./utilities/Middleware'); //Custom middleware
var handlebars = require('express-handlebars'), hbs;

// configuration ===========================================
var config = require('./config/config'); // config files
var db = require('./config/db'); // db
// Make our db accessible to our router
app.use(function (req, res, next) {
    req.db = db;
    next();
});

app.set('port', config[config.environment].application.port); //Port

app.use(bodyParser.json()); // parse application/json
app.use(bodyParser.urlencoded({ extended: true }));// parse application/x-www-form-urlencoded

app.use(express.static(path.join(__dirname, 'static'))); //static files folder

app.set('views', path.join(__dirname, 'views')); //configure views

//Sets handlebars view engine
hbs = handlebars.create({
    helpers:{
        ifCond: function(v1, operator, v2, options){
            v1 = v1.toString();
            v2 = v2.toString();
            switch (operator) {
                case '==':
                    return (v1 == v2) ? options.fn(this) : options.inverse(this);
                case '===':
                    return (v1 === v2) ? options.fn(this) : options.inverse(this);
                case '<':
                    return (v1 < v2) ? options.fn(this) : options.inverse(this);
                case '<=':
                    return (v1 <= v2) ? options.fn(this) : options.inverse(this);
                case '>':
                    return (v1 > v2) ? options.fn(this) : options.inverse(this);
                case '>=':
                    return (v1 >= v2) ? options.fn(this) : options.inverse(this);
                case '&&':
                    return (v1 && v2) ? options.fn(this) : options.inverse(this);
                case '||':
                    return (v1 || v2) ? options.fn(this) : options.inverse(this);
                default:
                    return options.inverse(this);
            }
        }
    },
    defaultLayout: 'main'
});
app.engine('handlebars', hbs.engine);
app.set('view engine', 'handlebars');

app.use(logger('dev', { format: 'dev', immediate: true })); //Logger
app.use(errorHandler({ dumpExceptions: true, showStack: true })); //Error handler
app.use(Middleware.AppendNotifications); //AppendNotifications
//App listener
http.createServer(app).listen(app.get('port'), function () {
console.log('Express server listening on port ' + app.get('port'));    
});

require('./router')(app); //configure routes